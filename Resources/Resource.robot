*** Settings ***
Documentation    Suite description
Library  Selenium2Library

*** Variables ***
${TEXT}         Dominos
${HOMEPAGE}     http://google.com.au
${ORDERTYPE}     id=pickup
${ORDERTIME}     css=#ordertimebutton > label.ordertimelater
${POSTCODE}     2166
${STORE}        css=#pickup-details > section > div > section > ul > li:nth-child(2) > a > h6
@{MY_LIST_VARIABLE}  Item1  Item2  Item3


*** Keywords ***
Log my multiple variables
    Log  @{MY_LIST_VARIABLE} [0]
    Log  @{MY_LIST_VARIABLE} [1]
    Log  @{MY_LIST_VARIABLE} [2]

Set a variable in the test case
    ${my_new_variable} =  set variable  This is a new variable

Search Text on Google
    go to  ${homepage}
    input text  id=lst-ib   ${TEXT}
#    click button  value="Google Search"
    Press Key  id=lst-ib  \\13

Select Dominos
    wait until element is visible  link=Domino's Online Ordering  20 Seconds
    click element  link=Domino's Online Ordering

Select pickup
    wait until element is visible  ${ORDERTYPE}
    click element  ${ORDERTYPE}

Select later
    wait until element is visible  ${ORDERTIME}
    click element  ${ORDERTIME}

Enter postcode
    input text  id=customer-suburb  ${POSTCODE}

Select store
    wait until page contains  Shops 1 & 2 135 Polding Street
    click element  ${STORE}

Select time
    wait until element is visible  id=order_time_select
    click element  id=order_time_select
    select from list by value  id=order_time_select  2017-12-15T17:30:00
    click element  id=start-order-button

Close Modal
    wait until page contains element  id=offer-title-3094
    click element  css=#offer-3094 > div.modal-dialog > div > button

Select a pizza
    wait until page contains element  id=product-image-menu-pizza-premium-P316
    click element  css=#product-menu-pizza-premium-P316 > a > div.form-container.input-group > div > div > div > button

Checkout
    click element  id=menu-basket

Log in


    


#Google and check results
#    [Arguments]  ${searchkey}   ${result}
#    input text    id=gs_lc0     ${searchkey}
#    click button  id=_fZl
#    wait until page contains  ${result}
#
#Go to homepage
#    open browser  ${HOMEPAGE}   ${BROWSER}





