*** Settings ***
Documentation    Suite description
Library  Selenium2Library

*** Variables ***

${ORDERTIME}     css=#ordertimebutton > label.ordertimelater
${POSTCODE}     2166
${STORE}        css=#pickup-details > section > div > section > ul > li:nth-child(2) > a > h6

*** Keywords ***

Select later
    wait until element is visible  ${ORDERTIME}
    click element  ${ORDERTIME}

Enter postcode
    input text  id=customer-suburb  ${POSTCODE}

Select store
    wait until page contains  Shops 1 & 2 135 Polding Street
    click element  ${STORE}










