*** Settings ***
Documentation    Suite description
Resource  ../Resources/Resource.robot
Resource  ../Resources/Common.robot

#Suite Setup  Open Browser Stack  BROWSER=Edge  BROWSER_VERSION=16  OS=Windows  OS_VERSION=10
Suite Setup  Launch Browser
Suite Teardown  Exit Browser

*** Test Cases ***

[TC-01] Search and navigate
    [Tags]  Smoke
    Search Text on Google
    Select Dominos

[TC-02] Select delivery type and time
    Select pickup
    Select later

[TC-03] Select Store
    Enter postcode
    Select store

[TC-04] Select pick up time
    Select time

[TC-05] Close Modal and Select Pizza
    Close Modal
    Select a pizza

[TC-06] Checkout and confirm details
    Checkout








